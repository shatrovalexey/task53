<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get( '/point' , 'Point@index' ) ;
Route::get( '/point/active' , 'Point@active' ) ;
Route::post( '/point/store' , 'Point@store' ) ;
Route::post( '/point/remove' , 'Point@remove' ) ;
Route::post( '/point/set' , 'Point@set' ) ;
Route::get( '/point/test' , 'Point@test' ) ;