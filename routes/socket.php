<?php

use App\Http\Sockets\Point ;

/*
 *  Routes for WebSocket
 *
 * Add route (Symfony Routing Component)
 */

$socket->route( '/point' , new Point( ) , [ '*' ] ) ;