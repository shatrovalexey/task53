<?php

namespace App\Http\Sockets;

use Orchid\Socket\BaseSocketListener;
use Ratchet\ConnectionInterface;
use App\Point as PointModel ;

class Point extends BaseSocketListener {
	/**
	* Current clients.
	*
	* @var SplObjectStorage
	* @var PointModel
	*/
	protected $clients ;
	protected $point ;

	/**
	* Point constructor.
	*/
	public function __construct( ) {
		$this->clients = new \SplObjectStorage( ) ;
		$this->point = new PointModel( ) ;
	}

	/**
	* @param ConnectionInterface $conn
	*/
	public function onOpen( ConnectionInterface $conn ) {
		$this->clients->attach( $conn ) ;
	}

    /**
     * @param ConnectionInterface $from
     * @param $msg
     */
    public function onMessage( ConnectionInterface $from , $msg ) {
        foreach ( $this->clients as $client ) {
		if ( $from != $client ) {
			continue ;
		}

		$points = $this->point->active( ) ;
                $client->send( $points ) ;
        }
    }

    /**
     * @param ConnectionInterface $conn
     */
    public function onClose(ConnectionInterface $conn)
    {
        $this->clients->detach($conn);
    }

    /**
     * @param ConnectionInterface $conn
     * @param \Exception          $e
     */
    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        $conn->close();
    }
}
