<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request ;
use App\Point as PointModel ;

class Point extends BaseController {
	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	/**
	* веб-представление
	* @return Response
	*/
	public function test( ) {
		return view( 'test' ) ;
	}

	/**
	* веб-представление
	* @return Response
	*/
	public function index( ) {
		return view( 'point' ) ;
	}

	/**
	* создать точку
	* @return integer - идентификатор
	*/
	public function store( Request $request ) {
		/**
		* @var string $fio - ФИО
		* @var integer $long - долгота
		* @var integer $lat - широта
		*/
		$fio = $request->input( 'fio' ) ;
		$long = $request->input( 'long' ) ;
		$lat = $request->input( 'lat' ) ;

		return PointModel::store( $fio , $long , $lat ) ;
	}

	/**
	* удалить точку
	* @returns integer - результат выполнения
	*/
	public function remove( Request $request ) {
		/**
		* @var integer $id - идентификатор
		*/

		$id = $request->input( 'id' ) ;

		return PointModel::remove( $id ) ;
	}

	/**
	* обновить точку
	* @returns integer - результат выполнения
	*/
	public static function set( Request $request ) {
		/**
		* @var integer $id - идентификатор
		* @var integer $lat - широта
		* @var integer $long - долгота
		*/

		$id = $request->input( 'id' ) ;
		$lat = $request->input( 'lat' ) ;
		$long = $request->input( 'long' ) ;

		return PointModel::set( $id , $lat , $long ) ;
	}

	/**
	* активные точки
	* @returns Response - результат выполнения
	*/
	public function active( ) {
		return PointModel::active( ) ;
	}
}
