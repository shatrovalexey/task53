<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Point extends Model
{
	protected $table = 'point' ;
	public $timestamps = false ;

	/**
	* все активные точки
	*/
	public static function active( ) {
		return self::select( 'lat' , 'long' , 'fio' )
			->where( 'active' , true )
			->orderBy( 'created' , 'DESC' )->get( ) ;
	}

	/**
	* создать точку
	* @param string $fio - ФИО
	* @param integer $long - долгота
	* @param integer $lat - широта
	* @returns integer - идентификатор
	*/
	public static function store( $fio , $long , $lat ) {
		$point = new self( ) ;
		$point->fio = $fio ;
		$point->long = $long ;
		$point->lat = $lat ;
		$point->active = true ;
		$point->save( ) ;

		return $point->id ;
	}

	/**
	* удалить точку
	* @param integer $id - идентификатор
	* @returns array
	*/
	public static function remove( $id ) {
		$point = self::find( $id ) ;

		if ( empty( $point ) ) {
			return [ 'result' => false ] ;
		}

		$point->active = false ;
		$point->inactive = DB::raw( 'now( )' ) ;
		$point->save( ) ;

		return [ 'result' => true ] ;
	}

	/**
	* удалить точку
	* @param integer $id - идентификатор
	*/
	public static function set( $id , $lat , $long ) {
		$point = self::find( $id ) ;

		if ( empty( $point ) ) {
			return [ 'result' => false ] ;
		}

		$point->lat = $lat ;
		$point->long = $long ;
		$point->save( ) ;

		return [ 'result' => true ] ;
	}
}
