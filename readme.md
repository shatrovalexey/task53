### ЗАДАЧА ###
```
1. Сервер должен быть реализован на языке программирования php, СУБД mysql
2. Должно быть реализовано 2 метода API, которые будет вызывать мобильное приложение
a. Добавить новую точку на карту. Должны передаваться значения: Широта, Долгота, ФИО клиента.
Данные параметры должны быть сохранены в БД, API должно вернуть идентификатор точки на
карте. Точка должна создаваться со статусом «Активна»
b. Деактивировать точку. Должно передаваться значение идентификатора точки. Результатом
выполнения должна стать смена статуса точки на «Не активна» в модели
3. Модель объекта точка
a. Идентификатор
b. ФИО клиента
c. Широта
d. Долгота
e. Дата и время создания
f. Дата и время деактивации
g. Точка активна?
4. Должна быть реализована отдельная страница с 2 блоками шириной 80 % и 20 %
a. В первом блоке необходимо разместить карту с использованием API Yandex карты. Должны быть
реализованы функции для масштабирования карты
b. Во втором блоке разместить список активных точек с отображением широты и долготы в виде
списка
5. Страница должна опрашивать БД на периодической основе, 1 раз в 2 секунды и отображать активные
точки на карте на основании полученных координат.
6. Интервал опроса точек должен быть конфигурируемый
```

### УСТАНОВКА И НАСТРОЙКА ###
* `apt-get update`
* `apt-get upgrade`
* `apt-get dist-upgrade`
* `apt-get install composer git htop nano nohup`
* `cd /var/www`
* `git clone https://bitbucket.org/shatrovalexey/task53.git`
* `cd task53`
* `composer install`
* `./artisan key:generate`
* `mysql`
* create database `task53` ;
* create user `task53`@`localhost` ;
* set password for `task53`@`localhost`  = password( 'f2ox9erm' ) ;
* grant allt privileges on `task53`.* to `task53`@`localhost` ;
* `flush privileges ;`
* `\q`
* `cp -p '.env.example' '.env'`
* настроить файл `.env`
* `DB_CONNECTION`
* `DB_HOST`
* `DB_PORT`
* `DB_DATABASE`
* `DB_USERNAME`
* `DB_PASSWORD`
* `SOCKET_HTTP_HOST`
* `SOCKET_ADDRESS`
* `SOCKET_PORT`
* `SOCKET_REFRESH`
* `YANDEX_MAP_API_KEY`
* `./artisan migrate`
* `./artisan db:seed --class=PointSeeder` - создать тестовые данные. Можно не создавать.

### ЗАПУСК ###
* `nohup ./artisan socket:serve &`
* `nohup ./artisan serve &`

### Веб-интерфейс ###
* `/point` - основной интерфейс
* `/point/test` - формы для выполнения методов API

### API ###
* `GET /point/active` - активные точки.
* `POST /point/store` - добавление точки. Аргументы: `fio`, `lat`, `long`.
* `POST /point/remove` - деактивация точки. Аргументы: `id`.
* `POST /point/set` - обновление координат точки. Аргументы: `id`, `lat`, `long`.

### АВТОР ###
Шатров Алексей Сергеевич <mail@ashatrov.ru>