jQuery( function( ) {
	let $socket ;

	let $socketSend = function( ) {
		try {
			$socket.send( true ) ;
		} catch ( $exception ) {
			console.log( $exception ) ;

			$socketCreate( ) ;
		}
	} ;
	let $socketClose = function( ) {
		$socketCreate( ) ;

		return true ;
	} ;
	let $socketMessage = function( $event ) {
		if ( $oldData == $event.data ) {
			return ;
		}

		$oldData = $event.data ;

		let $data ;

		$tbody.find( "tr:not(.nod)" ).remove( ) ;

		try {
			$data = JSON.parse( $event.data ) ;

			console.log( "parsed " + $data.length ) ;
		} catch( $exception ) {
			console.log( $exception ) ;

			return ;
		}

		if ( $map ) {
			$placemarks.removeAll( ) ;
		}

		jQuery( $data ).each( function( ) {
			let $rowCurrent = $row.clone( true ) ;

			for ( let $key in this ) {
				$rowCurrent.find( "*[data-col=" + $key + "]" ).text( this[ $key ] ) ;
			}

			$tbody.append( $rowCurrent ) ;

			if ( ! $map ) {
				console.log( "No map" ) ;

				return ;
			}

			let $placemark = new ymaps.Placemark( [ parseFloat( this.lat ) , parseFloat( this.long ) ] , {
				"balloonContent" : this.fio
			} ) ;

			$placemarks.add( $placemark ) ;
		} ) ;

		if ( ! $map ) {
			return ;
		}

		$map.geoObjects.add( $placemarks ) ;

		console.log( "Placemarks added: " + $placemarks.getLength( ) ) ;
	} ;

	let $socketCreate = function( ) {
		$socket = new WebSocket( "ws://" + location.hostname + ":8080/point" ) ;
	} ;

	$socketCreate( ) ;

	$socket.onclose = $socketClose ;
	$socket.onmessage = $socketMessage ;

	let $table = jQuery( "#points" ) ;
	let $tbody = $table.find( "tbody" ) ;
	let $row = $tbody.find( "tr.nod" ) ;
	let $oldData ;

	setInterval( $socketSend , $table.data( "refresh" ) ) ;
} ) ;

let $map , $placemarks ;
ymaps.ready( function init( ) { 
	// Создание карты.
	$map = new ymaps.Map( "map" , {
		// Координаты центра карты.
		// Порядок по умолчанию: «широта, долгота».
		// Чтобы не определять координаты центра карты вручную,
		// воспользуйтесь инструментом Определение координат.
		"center" : [ 43.12551231848849 , 131.9261712565307 ] ,
		// Уровень масштабирования. Допустимые значения:
		// от 0 (весь мир) до 19.
		"zoom": 12
       	} ) ;

	$placemarks = new ymaps.GeoObjectCollection( null , null ) ;
} ) ;
