<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title>Laravel</title>

		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" type="text/css">
		<link rel="stylesheet" href="/css/style.css" type="text/css">

		<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
		<script src="https://api-maps.yandex.ru/2.1/?apikey={{env( 'YANDEX_MAP_API_KEY' )}}&lang=ru_RU" type="text/javascript"></script>
		<script src="/js/point.js"></script>
	</head>
	<body>
		<table class="points-overlay">
			<tr>
				<td class="points-map">
					<div id="map"></div>
				</td>
				<td class="points-layout">
					<div>
					<table id="points" data-refresh="{{env( 'SOCKET_REFRESH' )}}">
						<thead>
							<tr>
								<!--th>#</th>
								<th>ФИО</th-->
								<th>Широта</th>
								<th>Долгота</th>
								<!--th>Создана</th-->
							</tr>
						</thead>
						<tbody>
							<tr class="nod">
								<!--td>
									<span data-col="id"></span>
								</td>
								<td>
									<span data-col="fio"></span>
								</td-->
								<td>
									<tt data-col="long"></tt>
								</td>
								<td>
									<tt data-col="lat"></tt>
								</td>
								<!--td>
									<span data-col="created"></span>
								</td-->
							</tr>
						</tbody>
					</table>
					</div>
				</td>
			</tr>
		</table>
	</body>
</html>
