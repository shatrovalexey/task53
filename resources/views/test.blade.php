<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title>Test</title>

		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" type="text/css">

		<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
		<script src="https://api-maps.yandex.ru/2.1/?apikey={{env( 'YANDEX_MAP_API_KEY' )}}&lang=ru_RU" type="text/javascript"></script>
	</head>
	<body>
		<h2>
			<a href="/point/active" target="_new">POST <tt>/point/active</tt></a>
		</h2>
		<form action="/point/store" method="post" target="_new">
			<fieldset>
				<legend>
					<h2>POST <tt>/point/store</tt></h2>
				</legend>
				<label>
					<span>ФИО:</span>
					<input name="fio" required>
				</label>
				<label>
					<span>широта:</span>
					<input name="lat" required>
				</label>
					<label>
					<span>долгота:</span>
					<input name="long" required>
				</label>
				<label>
					<span>выполнить</span>
					<input type="submit" value="&rarr;">
				</label>
			</fieldset>
		</form>
		<form action="/point/remove" method="post" target="_new">
			<fieldset>
				<legend>
					<h2>POST <tt>/point/remove</tt></h2>
				</legend>
				<label>
					<span>id:</span>
					<input name="id" required>
				</label>
				<label>
					<span>выполнить</span>
					<input type="submit" value="&rarr;">
				</label>
			</fieldset>
		</form>

	</body>
</html>
