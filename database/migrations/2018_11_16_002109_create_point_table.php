<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePointTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('point', function (Blueprint $table) {
		$table->bigIncrements( 'id' )
			->comment( 'Идентификатор' ) ;
		$table->string( 'fio' )
			->comment( 'ФИО клиента' ) ;
		$table->decimal( 'long' , 20 , 14 )
			->comment( 'Долгота' ) ;
		$table->decimal( 'lat' , 20 , 14 )
			->comment( 'Широта' ) ;
		$table->boolean( 'active' )->default( false )
			->comment( 'Точка активна?' ) ;
		$table->timestamp( 'inactive' )->nullable( ) ;
		$table->timestamp( 'created' )->useCurrent( ) ;

		$table->index( 'active' , 'created' ) ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('point');
    }
}
