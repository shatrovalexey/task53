<?php

use Illuminate\Database\Seeder;
use App\Point as PointModel ;

class PointSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run( ) {
	for ( $i = 0 ; $i < 100 ; $i ++ ) {
		PointModel::store( str_random( 10 ) , rand( -90e2 , 90e2 ) / 100. , rand( -180e2 , 180e2 ) / 100. ) ;
	}
    }
}
